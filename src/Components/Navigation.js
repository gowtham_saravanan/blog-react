import React from "react";
import {Container, Navbar, Nav, Button, NavLink} from "react-bootstrap";
import {getUserName, isAuthenticated} from "../services/Auth";
import {useSelector} from "react-redux";
import {Navigate, useLocation} from "react-router-dom";

function Navigation() {

    function logout() {
        localStorage.removeItem('user');
    }

    function getLoginText() {
        if (isAuthenticated()) {
            return (
                <Navbar.Text>
                    Signed in as: <span className="text-dark">{getUserName()}</span><a className="m-2" href="/login"
                                                                                       onClick={logout}>(logout)</a>
                </Navbar.Text>
            );
        }
        return <Navbar.Text><Nav.Link style={{textDecoration : "underline"}} href="/login">Login</Nav.Link></Navbar.Text>
    }

    return (
        <Navbar style={{backgroundColor: "#d8ebff"}} className="mb-4">
            <Container>
                <Navbar.Brand href="/">React Blog</Navbar.Brand>
                <Navbar.Toggle/>
                <Navbar.Collapse className="justify-content-end">
                    <Nav className="me-auto">
                        { isAuthenticated() ? <Nav.Link href="/articles">List Articles</Nav.Link> : '' }
                    </Nav>
                    {getLoginText()}
            </Navbar.Collapse>
        </Container>
</Navbar>
)
    ;
}

export default Navigation;