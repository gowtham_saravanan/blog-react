import {Card, Form} from "react-bootstrap";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {updateCategoryFilterAction, updateSearchFilterAction} from "../../Store/actions/ArticleActions";

function Filter() {
    const filter = useSelector(state => state.article.filter);
    const categories = useSelector(state => state.article.categories);
    const dispatch = useDispatch();

    return (
        <div className="col-12 col-lg-3 mb-4 p-0">
            <Card className="p-3 shadow-sm ">
                <span className="fw-bold fs-4 mb-3">Filters</span>

                <Form.Group controlId="search">
                    <Form.Control placeholder="Search .." value={filter.search}
                                onChange={e => dispatch(updateSearchFilterAction(e.target.value))}/>
                </Form.Group>
                <Form.Group controlId="category">
                    <label className="mt-3 mb-2">Category</label>
                    <Form.Select value={filter.category}
                                onChange={e => dispatch(updateCategoryFilterAction(e.target.value))}>
                        <option value="">Select A Category</option>
                        {categories.map(category => <option key={category.id}
                                                            value={category.id}>{category.title}</option>)}
                    </Form.Select>
                </Form.Group>
            </Card>
        </div>
        
    );

}

export default Filter;

