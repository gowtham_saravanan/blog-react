import React from "react";
import {Badge, Button, Card, Col} from "react-bootstrap";
import {getTrimmedArticleDescription} from "../../services/Helpers";
import {removeArticleAction, updateDeleteRequestAction} from "../../Store/actions/ArticleActions";
import {useDispatch} from "react-redux";

function Article({title, body, category_title, id}) {

    function editArticle(id) {
        window.location.replace('/articles/edit/' + id);
    }

    const dispatch = useDispatch();

    return (
        <>
            <Col md={4} className="mb-3">
                <Card style={{minHeight: '200px'}} className="shadow-sm">
                    <Card.Body>
                        <span className="fw-bold fs-4">{title}</span>
                        <p>{getTrimmedArticleDescription(body)}</p>
                        <div className="d-flex flex-row justify-content-around flex-wrap gap-2">
                            <div>
                                <Badge bg="secondary">{category_title}</Badge>
                            </div>
                            <div>
                                <Button onClick={() => editArticle(id)} variant="outline-secondary">Edit</Button>
                                <Button onClick={() => {
                                    dispatch(updateDeleteRequestAction('started'));
                                    dispatch(removeArticleAction({id: id}))
                                }} variant="outline-danger"
                                        className="ms-1">Delete</Button>
                            </div>
                        </div>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );

}

export default Article;