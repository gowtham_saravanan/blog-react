import React, {useEffect, useRef, useState} from "react";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {createArticleAction, updateCreateRequestAction} from "../../Store/actions/ArticleActions";

function AddArticle(props) {

    const [title, setTitle] = useState('');
    const [category, setCategory] = useState(0);
    const [body, setBody] = useState('');

    const categories = useSelector(state => state.article.categories);
    const requestStatus = useSelector(state => state.article.request.create.status)
    const error = useSelector(state => state.article.request.create.errors)

    const dispatch = useDispatch();
    const submitButton = useRef();

    function createArticle(e) {
        e.preventDefault();
        dispatch(updateCreateRequestAction('started'));
        dispatch(createArticleAction(title, category, body));
    }

    useEffect(() => {

        if (requestStatus === 'started') {
            submitButton.current.disabled = true;
            return;
        }

        if (requestStatus === 'completed') {
            setTitle('');
            setBody('');
            setCategory(0);
        }

        submitButton.current.disabled = false;
    }, [requestStatus])

    return (
        <div className="mx-4">
            <Card className="shadow-sm" >
                <Card.Header>Add Article</Card.Header>
                <Card.Body className="p-3">
                    <Form onSubmit={createArticle}>
                        <Row className="mb-3">
                            <Form.Group as={Col} controlId="title">
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" placeholder="Enter Title" value={title} isInvalid={!!error.title}
                                              onChange={(e) => setTitle(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.title}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group as={Col} controlId='category'>
                                <Form.Label>Category</Form.Label>
                                <Form.Select value={category} onChange={(e) => setCategory(e.target.value)}>
                                    <option value="0">Select A Category</option>
                                    {categories.map(category => <option key={category.id}
                                                                        value={category.id}>{category.title}</option>)}
                                </Form.Select>
                            </Form.Group>
                        </Row>

                        <Form.Group className="mb-3" controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control placeholder="Enter Description" value={body} isInvalid={!!error.body}
                                          onChange={e => setBody(e.target.value)}/>
                            <Form.Control.Feedback type="invalid">
                                {error.body}
                            </Form.Control.Feedback>
                        </Form.Group>

                        <Button variant="outline-primary" type="submit" ref={submitButton}>
                            {requestStatus === 'started' ? 'Publishing...' : 'Publish'}
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </div>
    );
}

export default AddArticle;