import React, {useEffect, useRef, useState} from "react";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import {useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {
    editArticleAction,
    getArticleAction,
    getCategoriesAction,
    updateEditRequestAction
} from "../../Store/actions/ArticleActions";

function EditArticle(props) {

    const [title, setTitle] = useState('');
    const [category, setCategory] = useState(0);
    const [body, setBody] = useState('');

    const categories = useSelector(state => state.article.categories);
    const currentArticle = useSelector(state => state.article.current);
    const editRequestStatus = useSelector(state => state.article.request.edit.status)
    const error = useSelector(state => state.article.request.create.errors)


    const {id} = useParams();
    const dispatch = useDispatch();
    const updateButton = useRef();

    useEffect(() => {
        setTitle(currentArticle.title);
        setBody(currentArticle.body);
        setCategory(currentArticle.category);
    }, [currentArticle]);

    useEffect(() => {
        dispatch(getCategoriesAction());
        dispatch(getArticleAction(id));
    }, [])

    useEffect(() => {
        if (editRequestStatus === 'started') {
            updateButton.current.disabled = true;
        }
        updateButton.current.disabled = false;

    }, [editRequestStatus]);

    function updateArticle(e) {
        e.preventDefault();
        dispatch(updateEditRequestAction('started'));
        dispatch(editArticleAction(id, title, category, body));
    }

    return (
        <>
            <div className="mx-4">
                <Card className="shadow-sm">
                    <Card.Header>Edit Article</Card.Header>
                    <Card.Body>
                        <Form onSubmit={updateArticle}>
                            <Row className="mb-3">
                                <Form.Group as={Col} controlId="title">
                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" placeholder="Enter Title" value={title} isInvalid={!!error.title}
                                                  onChange={(e) => setTitle(e.target.value)}/>
                                    <Form.Control.Feedback type="invalid">
                                        {error.title}
                                    </Form.Control.Feedback>
                                </Form.Group>

                                <Form.Group as={Col} controlId='category'>
                                    <Form.Label>Category</Form.Label>
                                    <Form.Select value={category} onChange={(e) => setCategory(e.target.value)}>
                                        <option value="0">Select A Category</option>
                                        {categories.map(category => <option key={category.id}
                                                                            value={category.id}>{category.title}</option>)}
                                    </Form.Select>
                                </Form.Group>
                            </Row>

                            <Form.Group className="mb-3" controlId="description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control placeholder="Enter Description" value={body} isInvalid={!!error.body}
                                              onChange={e => setBody(e.target.value)}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.body}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Button variant="outline-primary" type="submit" ref={updateButton}>
                                {editRequestStatus === 'started' ? 'Updating...' : 'Update'}
                            </Button>

                            <Button className="ms-4" onClick={() => window.location.replace('/articles')}
                                    variant="outline-secondary" type="button">Go Back</Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </>
    );
}

export default EditArticle;