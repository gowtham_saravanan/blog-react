import React, {useEffect} from "react";
import Filter from "./Filter";
import Article from "./Article";
import {Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {getArticlesAction} from "../../Store/actions/ArticleActions";


function ListArticles(props) {

    const articles = useSelector(state => state.article.articles);
    const dispatch = useDispatch();

    const createRequestStatus = useSelector(state => state.article.request.create.status);
    const deleteRequestStatus = useSelector(state => state.article.request.delete.status);
    const filter = useSelector(state => state.article.filter);


    useEffect(() => {
        if (createRequestStatus === 'completed' || deleteRequestStatus === 'completed') {
            dispatch(getArticlesAction(filter));
        }
    }, [createRequestStatus, deleteRequestStatus]);


    useEffect(() => {
        dispatch(getArticlesAction(filter));
    }, [filter]);

    return (
        <div className="row m-4">
            <Filter/>
            <Row className="ms-2 col-12 col-lg-9 ">
                {articles.map(article => <Article key={article.id} {...article} />)}
            </Row>
        </div>
    );
}

export default ListArticles;