import {Toast, ToastContainer} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {hideAlertAction} from "../../Store/actions/ArticleActions";
import {useEffect} from "react";

function CustomAlert() {

    const alert = useSelector(state => state.article.alert);
    const dispatch = useDispatch();

    useEffect(() => {
        if (alert.show) {
            setTimeout(() => {
                dispatch(hideAlertAction());
            }, 3000)
        }

    }, [alert]);

    function getAlertColor(){
        return alert.type==='Success'?'lightgreen':'lightyellow';
    }

    if (alert.show) {
        return (
            <ToastContainer position="top-end" style={{zIndex: 100}}>
                <Toast onClose={() => dispatch(hideAlertAction())}>
                    <Toast.Header style={{backgroundColor : getAlertColor()}}>
                        <strong className="me-auto">{alert.type}</strong>
                        <small className="text-muted">just now</small>
                    </Toast.Header>
                    <Toast.Body>{alert.message}</Toast.Body>
                </Toast>
            </ToastContainer>
        );
    }

    return <></>;

}

export default CustomAlert;