import {Navigate, Outlet, useLocation} from 'react-router-dom';
import {isAuthenticated} from "../../services/Auth";

const PrivateRoutes = () => {
    const location = useLocation();

    return isAuthenticated()
        ? <Outlet />
        : <Navigate to="/login" replace state={{ from: location }} />;
}

export default PrivateRoutes;