import React, {useState} from "react";
import {Button, Card, Form} from "react-bootstrap"
import Api from "../services/Api";
import {store} from "../Store/store";
import {storeUser} from "../Store/actions/AuthActions";

function Register() {
    const [submitStatus, setSubmitStatus] = useState({'disabled' : false});

    const errorObject = {
        message: {
            'name': '',
            'email': '',
            'password': ''
        },
        validity: {
            'name': {'valid': "true"},
            'email': {'valid': "true"},
            'password': {'valid': "true"},
        }
    };

    const authorObject = {
        'name': '',
        'email': '',
        'password': '',
        'confirm_password': ''
    };

    const [error, setError] = useState({...errorObject});

    const [author, setAuthor] = useState({...authorObject});

    function updateChange(e) {
        setAuthor({...author, [e.target.id]: e.target.value});
    }

    function register(e) {
        e.preventDefault();
        setError({...errorObject});


        if (author.password !== author.confirm_password) {
            setError({
                ...error,
                message: {...error.message, password: "Passwords do not match"},
                validity: {...error.validity, password: {"isInvalid": "true"}}
            });
            return;
        }

        setSubmitStatus({'disabled': true});

        Api.post('/register', {...author}, {'headers': {'Content-Type': 'application/json'}})
            .then(res => {
                res.data.author && store.dispatch(storeUser(res.data.author));
                setError({...errorObject});
                setAuthor({...authorObject})
                setSubmitStatus({'disabled': false});
            }).catch((e) => {
            handleErrorResponse(e);
        });
    }

    function handleErrorResponse(e) {
        let errors = e.response.data.errors;
        if (e.response.status === 422 && errors) {
            let message = {}
            let validity = {}

            Object.keys(errors).forEach(field => {
                message[field] = errors[field][0];
                validity[field] = {"isInvalid": "true"};
            });

            setError({
                ...error,
                message: {...errorObject.message, ...message},
                validity: {...errorObject.validity, ...validity}
            });

            setSubmitStatus({'disabled': false});
        }
    }

    return (
        <>
            <div className="App d-flex justify-content-center align-items-center bg-light">
                <Card className="w-50">
                    <Card.Title className="p-2 mt-3 text-center"> SignUp </Card.Title>
                    <Card.Body>
                        <Form onSubmit={register}>
                            <Form.Group className="mb-3" controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="Enter name"
                                              value={author.name} onChange={updateChange} {...error.validity.name}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.message.name}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" required placeholder="Enter email"
                                              value={author.email} onChange={updateChange} {...error.validity.email}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.message.email}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" required placeholder="Password"
                                              value={author.password}
                                              onChange={updateChange} {...error.validity.password}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.message.password}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="confirm_password">
                                <Form.Label>Password Confirmation</Form.Label>
                                <Form.Control type="password" required placeholder="Password Confirmation"
                                              value={author.confirm_password} onChange={updateChange}/>
                            </Form.Group>

                            <Button variant="primary" type="submit" {...submitStatus} >
                                {submitStatus.disabled ? 'Loading...' : 'Register'}
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </>
    );
}

export default Register;