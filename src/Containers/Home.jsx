import {Card} from "react-bootstrap";

function Home() {
    return (
        <div>
            <h3 className="text-center">React Basic CRUD</h3>
            <p className="text-center">This is a blog application, where users can login and manage their articles. It
                also contains filters and search option.</p>
            <div className="d-flex flex-row justify-content-center flex-wrap">
                <Card className="m-2">
                    <Card.Header>
                        Features
                    </Card.Header>
                    <Card.Body>
                        <ul>
                            <li>Authentication</li>
                            <li>Add Article</li>
                            <li>Listing Article with category and search filter option</li>
                            <li>Delete Article</li>
                            <li>Edit Article</li>
                            <li>List, Add, Edit and Delete are only accessible by logged in users</li>
                        </ul>
                    </Card.Body>
                </Card>
                <Card className="m-2">
                    <Card.Header>
                        Packages Used
                    </Card.Header>
                    <Card.Body>
                        <ul>
                            <li>Bootstrap</li>
                            <li>React Bootstrap</li>
                            <li>Redux</li>
                            <li>React Redux</li>
                            <li>Axios</li>
                            <li>Thunk</li>
                            <li>React Router</li>
                        </ul>
                    </Card.Body>
                </Card>

                <Card className="m-2">
                    <Card.Header>
                        React Hooks Used
                    </Card.Header>
                    <Card.Body>
                        <ul>
                            <li>useState()</li>
                            <li>useEffect()</li>
                            <li>useRef()</li>
                            <li>useSelector()</li>
                            <li>useDispatch()</li>
                        </ul>
                    </Card.Body>
                </Card>
            </div>
            <div className="text-center pt-2">
                <a href="https://gitlab.com/gowtham_saravanan/blog-react">
                    <img width="25px" src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png"/>Github
                    Link</a>
            </div>
        </div>
    );
}

export default Home;