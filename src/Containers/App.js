import Register from "./Register";
import Login from "./Login";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom"
import Navigation from "../Components/Navigation";
import Article from "./Article";
import EditArticle from "../Components/Article/EditArticle";
import CustomAlert from "../Components/Util/CustomAlert";
import {useDispatch} from "react-redux";
import {isAuthenticated} from "../services/Auth";
import ProtectedRoutes from "../Components/Util/ProtectedRoutes";
import Home from "./Home";

function App() {
    return (
        <div className="App bg-light">
            <Navigation/>
            <CustomAlert/>
            <Router>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/register" element={<Register/>}/>
                    <Route path="/" element={<ProtectedRoutes/>}>
                        <Route path="/articles" element={<Article/>}/>
                        <Route path="/articles/edit/:id" element={<EditArticle/>}/>
                    </Route>
                </Routes>
            </Router>
        </div>
    );
}

export default App;
