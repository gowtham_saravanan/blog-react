import React, {useEffect, useState} from "react";
import AddArticle from "../Components/Article/AddArticle";
import ListArticles from "../Components/Article/ListArticles";
import {useDispatch} from "react-redux";
import {getCategoriesAction} from "../Store/actions/ArticleActions";

function Article() {

    const [reloadArticles, setReloadArticles] = useState(0);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCategoriesAction());
    }, []);

    return (
        <div className="">
            <AddArticle reload={() => setReloadArticles(reloadArticles + 1)}/>
            <ListArticles key={reloadArticles}/>
        </div>
    );
}

export default Article;