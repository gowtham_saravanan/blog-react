import React, {useEffect, useRef, useState} from "react";
import {Button, Card, Form} from "react-bootstrap"
import Api from "../services/Api";
import {store} from "../Store/store";
import {loginAction, storeUser, updateLoginRequestStatusAction} from "../Store/actions/AuthActions";
import {useDispatch, useSelector} from "react-redux";

function Login() {
    const authorObject = {
        'email': 'gowtham@gmail.com',
        'password': '12345678',
    };

    const [author, setAuthor] = useState({...authorObject});
    const requestStatus = useSelector(state => state.auth.request.login.status);
    const error = useSelector(state => state.auth.request.login.error);
    const dispatch = useDispatch();
    const submitButton = useRef();

    function updateChange(e) {
        setAuthor({...author, [e.target.id]: e.target.value});
    }

    function login(e) {
        e.preventDefault();
        dispatch(updateLoginRequestStatusAction('started'));
        dispatch(loginAction(author));
    }

    useEffect(() => {
        if(requestStatus === 'completed'){
            window.location.replace('/articles');
        }
    }, [requestStatus])

    return (
            <div className="h-75 row justify-content-center align-items-center">
                <Card className="shadow-sm col-10 col-sm-5">
                    <Card.Title className="p-2 mt-3 text-center"> Login </Card.Title>
                    <Card.Body>
                        <Form onSubmit={login}>
                            <Form.Group className="mb-3" controlId="email">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="text"  placeholder="Enter email" isInvalid={!!error.email}
                                              value={author.email} onChange={updateChange}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.email}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="text"  placeholder="Password" value={author.password}
                                              onChange={updateChange} isInvalid={!!error.password}/>
                                <Form.Control.Feedback type="invalid">
                                    {error.password}
                                </Form.Control.Feedback>
                            </Form.Group>

                            <Button variant="outline-primary" type="submit" ref={submitButton} >
                                {requestStatus === 'started' ? 'Logging in...' : 'Login'}
                            </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
    );
}

export default Login;