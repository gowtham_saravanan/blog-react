import axios from "axios";

export default axios.create({
    baseURL:'https://blog-api-for-react.herokuapp.com/api'
});