
export function getTrimmedArticleDescription(str) {
    return str.split(/\s+/).slice(0,50).join(" ") + "...";
}
