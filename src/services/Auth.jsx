import Api from "./Api";
import {store} from "../Store/store";


export function signup({name, email, password}){
    return Api.post('/register', {name, email, password})
}

export function login({email, password}){
    return Api.post('/login', {email, password})
}

export function getToken(){
    return getUser() ? getUser().api_token : '';
}

export function saveTokenInLocalStorage(user){
    localStorage.setItem('user', JSON.stringify(user));
}

export function getUserName(){
    return getUser() ? getUser().name : '';
}

export function isAuthenticated(){
    return !!localStorage.getItem('user');
}

export function getUser(){
    return localStorage.getItem('user') == 'undefined' || localStorage.getItem('user') === '' ? {} : JSON.parse(localStorage.getItem('user'));
}