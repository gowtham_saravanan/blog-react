import Api from "./Api";
import {getToken} from "./Auth";

const config = {
    headers: { Authorization: `Bearer ${getToken()}` }
};

export function getCategories(){
    return Api.get('/categories', config);
}

export function getArticles(params = {}){
    const config = {
        params : {...params},
        headers: { Authorization: `Bearer ${getToken()}` }
    }

    return Api.get('/articles', config);
}

export function getArticle(id){
    return Api.get('/article/'+ id+ '/show', config);
}

export function createArticle({title,category,body}){
    return Api.post('/article/store',{title,category,body},config)
}

export function editArticle({id,title,category,body}){
    return Api.post('/article/update',{id, title,category,body}, config)
}

export function removeArticle(params){
    return Api.post('/article/remove',{...params} ,config);
}
