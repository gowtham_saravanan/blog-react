import {login, saveTokenInLocalStorage} from "../../services/Auth";
import {showAlertAction} from "./ArticleActions";

export const STORE_USER = 'store_user';

export const UPDATE_LOGIN_REQUEST_STATUS = 'update_login_request_status';
export const UPDATE_LOGIN_ERROR = 'update_login_error';


export function storeUser(user){
    return {
        'type': STORE_USER,
        'payload': user
    }
}

export function loginAction(author){
    return (dispatch) => {
        login(author).then(response => {
            saveTokenInLocalStorage(response.data.author);
            dispatch(storeUser(response.data.author));
            dispatch(showAlertAction('Success', 'Login Successful! Redirecting to articles.'))
            dispatch(updateLoginRequestStatusAction('completed'));
            dispatch(updateLoginErrorAction({}));
        }).catch(error => {
            dispatch(showAlertAction('Failure', 'Login Failed'))
            dispatch(updateLoginRequestStatusAction('failed'));
            if(error.response.status === 422 ){
                dispatch(updateLoginErrorAction(error.response.data.errors));
                return;
            }
            dispatch(updateLoginErrorAction({}));

        })
    }
}

export function updateLoginRequestStatusAction(status){
    return {
        'type' : UPDATE_LOGIN_REQUEST_STATUS,
        'payload' : status
    }
}

export function updateLoginErrorAction(errors){
    return {
        'type' : UPDATE_LOGIN_ERROR,
        'payload' : errors
    }
}