import {
    createArticle,
    editArticle,
    getArticle,
    getArticles,
    getCategories,
    removeArticle
} from "../../services/Article";

export const UPDATE_ARTICLES = 'update_articles';
export const UPDATE_CURRENT_ARTICLE = 'update_current_article';
export const UPDATE_CREATE_REQUEST_STATUS = 'update_create_request_status';
export const UPDATE_DELETE_REQUEST_STATUS = 'update_delete_request_status';
export const UPDATE_EDIT_REQUEST_STATUS = 'update_edit_request_status';

export const UPDATE_CREATE_ERROR = 'update_create_error';
export const UPDATE_EDIT_ERROR = 'update_edit_error';
export const UPDATE_DELETE_ERROR = 'update_delete_error';

export const UPDATE_CATEGORIES = 'update_categories';
export const SHOW_ALERT = 'show_alert';
export const HIDE_ALERT = 'hide_alert';

export const UPDATE_CATEGORY_FILTER = 'update_category_filter';
export const UPDATE_SEARCH_FILTER = 'update_search_filter';

// Get Categories
export function getCategoriesAction() {
    return (dispatch, useState) => {
        getCategories().then(
            response => {
                dispatch(storeCategoriesAction(response.data.data))
            }).catch(error => {
            if (error.response.status === 401) {
                console.log('Redirect to login');
            }
            dispatch(showAlertAction('Failure', 'Creating Article Failed!'))
        })
    }
}

export function storeCategoriesAction(categories) {
    return {
        'type': UPDATE_CATEGORIES,
        'payload': categories
    };
}

// Alert
export function showAlertAction(type, message) {
    return {
        'type': SHOW_ALERT,
        'payload': {
            'type': type,
            'message': message
        }
    }
}

export function hideAlertAction() {
    return {
        'type': HIDE_ALERT,
    };
}

// Create

export function createArticleAction(title, category, body) {

    return (dispatch) => {
        createArticle({title, category, body}).then(response => {
            dispatch(updateCreateRequestAction('completed'));
            dispatch(showAlertAction('Success', 'Article Created Successfully!'))
            dispatch(updateCreateErrorAction({}))

        }).catch(error => {
            dispatch(updateCreateRequestAction('failed'));
            dispatch(showAlertAction('Failure', 'Creating Article Failed!'))

            if(error.response.status === 422){
                dispatch(updateCreateErrorAction(error.response.data.errors))
                return;
            }
            dispatch(updateCreateErrorAction({}))


        });
    }
}

export function updateCreateRequestAction(status) {
    return {
        'type': UPDATE_CREATE_REQUEST_STATUS,
        'payload': status
    }
}

export function updateCreateErrorAction(errors) {
    return {
        'type': UPDATE_CREATE_ERROR,
        'payload': errors
    }
}

// Update
export function editArticleAction(id, title, category, body) {

    return (dispatch) => {
        editArticle({id, title, category, body}).then(response => {
            dispatch(updateEditRequestAction('completed'));
            dispatch(showAlertAction('Success', 'Article Edited Successfully!'))
            dispatch(updateEditErrorAction({}))
        }).catch(error => {
            dispatch(updateEditRequestAction('failed'));
            dispatch(showAlertAction('Failure', 'Creating Editing Failed!'))

            if(error.response.status === 422){
                dispatch(updateEditErrorAction(error.response.data.errors))
                return;
            }
            dispatch(updateEditErrorAction({}))
        });
    }
}


export function updateEditRequestAction(status) {
    return {
        'type': UPDATE_EDIT_REQUEST_STATUS,
        'payload': status
    }
}

export function updateEditErrorAction(errors) {
    return {
        'type': UPDATE_EDIT_ERROR,
        'payload': errors
    }
}

// Delete

export function removeArticleAction(params) {
    return (dispatch) => {
        removeArticle(params).then(response => {
            dispatch(updateDeleteRequestAction('completed'));
            dispatch(showAlertAction('Success', 'Article Deleted Successfully!'))
        }).catch(error => {
            dispatch(updateDeleteRequestAction('failed'));
            dispatch(showAlertAction('Failure', 'Fetching Articles Failed!'))
        });
    }
}

export function updateDeleteRequestAction(status) {
    return {
        'type': UPDATE_DELETE_REQUEST_STATUS,
        'payload': status
    }
}

// Get Articles

export function getArticlesAction(params = {}) {
    return (dispatch) => {
        getArticles(params).then(response => {
            dispatch(storeArticlesAction(response.data.data));
        }).catch(error => {
            dispatch(showAlertAction('Failure', 'Fetching Articles Failed!'))
        });
    }
}

export function storeArticlesAction(articles) {
    return {
        'type': UPDATE_ARTICLES,
        'payload': articles
    }
}

// Get Article

export function getArticleAction(id) {
    return (dispatch) => {
        getArticle(id).then(response => {
            dispatch(storeCurrentArticleAction(response.data.data))
        }).catch(error => {
            dispatch(showAlertAction('Failure', 'Fetching Article Failed!'))
        })
    }
}

export function storeCurrentArticleAction(article) {
    return {
        'type': UPDATE_CURRENT_ARTICLE,
        'payload': article
    }
}

// Filter

export function updateCategoryFilterAction(category) {
    return {
        'type': UPDATE_CATEGORY_FILTER,
        'payload': category
    }
}

export function updateSearchFilterAction(search) {
    return {
        'type': UPDATE_SEARCH_FILTER,
        'payload': search
    }
}

