import {
    HIDE_ALERT,
    SHOW_ALERT,
    UPDATE_ARTICLES,
    UPDATE_CATEGORIES,
    UPDATE_CATEGORY_FILTER, UPDATE_CREATE_ERROR,
    UPDATE_CREATE_REQUEST_STATUS,
    UPDATE_CURRENT_ARTICLE,
    UPDATE_DELETE_REQUEST_STATUS, UPDATE_EDIT_ERROR,
    UPDATE_EDIT_REQUEST_STATUS,
    UPDATE_SEARCH_FILTER
} from "../actions/ArticleActions";

const initialState = {
    'categories': [],
    'articles': [],
    'current': {},
    'alert': {
        'show': false,
        'type': '',
        'message': ''
    },
    'filter': {
        'search': '',
        'category': ''
    },
    'request' : {
        'create' : {
          'status' : '',
          'errors' : {},
        },
        'edit' : {
            'status' : '',
            'errors' : {},
        },
        'delete' : {
            'status' : '',
            'errors' : {},
        },
    },
    'create_request_status': '',
    'edit_request_status': '',
    'delete_request_status': '',
}

export function ArticleReducer(state = initialState, action) {

    switch (action.type) {
        case UPDATE_CATEGORIES:
            return {...state, categories: action.payload}

        case UPDATE_ARTICLES:
            return {...state, articles: action.payload}

        case UPDATE_CURRENT_ARTICLE:
            return {...state, current: action.payload}

        case SHOW_ALERT:
            return {
                ...state,
                alert: {...state.alert, show: true, type: action.payload.type, message: action.payload.message}
            };

        case HIDE_ALERT:
            return {...state, alert: {...state.alert, show: false, type: '', message: ''}};

        case UPDATE_CREATE_REQUEST_STATUS:
            let createStatus = {...state.request.create, status : action.payload}
            return {...state, request : {...state.request, create: createStatus}};

        case UPDATE_CREATE_ERROR:
            let createErrorMessage = {};
            Object.keys(action.payload).forEach(field => {
                createErrorMessage[field] = action.payload[field][0];
            });
            return {...state, request : {...state.request, create: {...state.request.create, errors : createErrorMessage}}};

        case UPDATE_EDIT_REQUEST_STATUS:
            let editStatus = {...state.request.edit, status : action.payload}
            return {...state, request : {...state.request, edit: editStatus}};

        case UPDATE_EDIT_ERROR:
            let editErrorMessage = {};
            Object.keys(action.payload).forEach(field => {
                editErrorMessage[field] = action.payload[field][0];
            });
            return {...state, request : {...state.request, create: {...state.request.create, errors : editErrorMessage}}};

        case UPDATE_DELETE_REQUEST_STATUS:
            let deleteStatus = {...state.request.delete, status : action.payload}
            return {...state, request : {...state.request, delete: deleteStatus}};

        case UPDATE_CATEGORY_FILTER:
            return {...state, filter: {...state.filter, category: action.payload}}

        case UPDATE_SEARCH_FILTER:
            return {...state, filter: {...state.filter, search: action.payload}}
    }

    return state;

}