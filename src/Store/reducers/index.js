import {ArticleReducer} from "./ArticleReducer";
import {AuthReducer} from "./AuthReducer";
import {combineReducers} from "redux";

const allReducers = combineReducers({
    article: ArticleReducer,
    auth: AuthReducer
})
export default allReducers