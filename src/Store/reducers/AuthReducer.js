import {STORE_USER, UPDATE_LOGIN_ERROR, UPDATE_LOGIN_REQUEST_STATUS} from "../actions/AuthActions";
import {getUser, saveTokenInLocalStorage} from "../../services/Auth";

const initialState = {
    'details': getUser(),
    'request': {
        'login': {
            'status': '',
            'error': {},
        }
    }
}

export function AuthReducer(state = initialState, action) {

    switch (action.type) {
        case STORE_USER:
            return {...state, user: action.payload};
        case UPDATE_LOGIN_REQUEST_STATUS:
            return {...state, request:{...state.request, login:{...state.request.login, status: action.payload}}};
        case UPDATE_LOGIN_ERROR:
            let loginError = {};
            Object.keys(action.payload).forEach(field => {
                loginError[field] = action.payload[field][0];
            });
            return {...state, request:{...state.request, login:{...state.request.login, error: loginError}}};
        default:
            return state;
    }
}