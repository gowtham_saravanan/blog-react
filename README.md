# React Blog

This is a blog application, where users can login and manage their articles. It also contains filters and search option.

## Hosted Link

[https://blog-reactprojects.netlify.app/] (gowtham@gmail.com/12345678)

## Features 

- Authentication
- Add Article
- Listing Article with category and search filter option
- Delete Article
- Edit Article
- List, Add, Edit and Delete are only accessible by logged in users

## Packages Used

- Bootstrap
- React Bootstrap
- Redux
- React Redux
- Axios
- Thunk
- React Router

## React Hooks Used

- useState()
- useEffect()
- useRef()
- useSelector()
- useDispatch()
